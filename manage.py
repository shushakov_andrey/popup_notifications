#!/usr/bin/env python
import os
import sys

p = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, '%s/popup_notification' % p)
sys.path.insert(0, '%s/popup_notification/apps' % p)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "popup_notification.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
