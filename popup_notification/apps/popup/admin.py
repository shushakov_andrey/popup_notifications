from django.conf.urls import patterns, url, include
from django.contrib import admin
from django.contrib.sessions.models import Session
from django.http import HttpResponse, HttpResponseRedirect


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']

admin.site.register(Session, SessionAdmin)


# Register your models here.
