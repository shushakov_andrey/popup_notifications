# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('text', models.TextField()),
                ('creation_date', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'ordering': ('-creation_date',),
                'verbose_name': 'post',
                'verbose_name_plural': 'posts',
            },
        ),
    ]
