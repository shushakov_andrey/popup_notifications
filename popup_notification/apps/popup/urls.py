from django.conf.urls import url
from popup import views

urlpatterns = [
    url(r'^$', views.index, name='index'),    
    url(r'^get_all_notifications/$',  views.get_notifications, name='get_all_notifications'),
    url(r'^get_last_5_notifications/$',  views.get_notifications, kwargs={'get_last_5': True}, name='get_last_5_notifications'),
    url(r'^auto_create_notifications/$',  views.auto_create_notifications, name='auto_create_notifications'),

    url(r'^activate_view_notifications/$',  views.toggle_view_notifications, kwargs={'activate':True}, name='activate_view_notifications'),        
    url(r'^deactivate_view_notifications/$',  views.toggle_view_notifications, kwargs={'deactivate':True}, name='deactivate_view_notifications'),
]
