# -*- coding: cp1251 -*-
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import loader, RequestContext

from datetime import datetime
import json


def index(request):    

    context = dict()

    # по умолчанию отображение уведомлений отключено
    request.session['activate_notifications'] = False

    return render_to_response('popup/index.html', context, context_instance=RequestContext(request))

# автоматическое создание уведомлений. для разнообразия задаются разные типы
def auto_create_notifications(request):             

    if int(datetime.now().strftime('%S'))%2:
    	messages.info(request, '>>> ' + str(datetime.now()))    
    else:
    	messages.success(request, '>>> ' + str(datetime.now()))    

    if int(datetime.now().strftime('%M'))%2:
    	messages.warning(request, '>>> ' + str(datetime.now()))    
    else:
    	messages.error(request, '>>> ' + str(datetime.now()))    

    return HttpResponse('Message created!')

# универсальный метод, забирающий уведомления из сессии
def get_notifications(request, **kwargs):    

    all_notifications = list()

    # если не было разрешения из админки, то не отображаем уведомления и возвращаем пустой список
    if request.session.get('activate_notifications', ''):
	    storage = messages.get_messages(request)        

	    # забираем уведомления из текущей сессии. отображенные уведомления удаляются из сессии (кроме 5 последних)
	    for message in storage:        
	        notification = dict()                    
	        notification['tag'] = message.tags
	        notification['message'] = message.message
	        all_notifications.append(notification)

	        # последние пять уведомлений храним в отдельном объекте
	        if not request.session.get('last_5_notifications', ''):            
	            request.session['last_5_notifications'] = list()

	        # т.к. нам нужно только 5 последних уведомлений, то удаляем старые (из начала списка)
	        while len(request.session['last_5_notifications']) > 4: 
	        	del request.session['last_5_notifications'][0]        

	        request.session['last_5_notifications'].append(notification)	   
	
    if 'get_last_5' in kwargs:                        
        return HttpResponse(json.dumps(request.session['last_5_notifications']), content_type="text/javascript; charset=UTF-8")    

    return HttpResponse(json.dumps(all_notifications), content_type="text/javascript; charset=UTF-8")

# флаг, отвечающий за отображение уведомлений
def toggle_view_notifications(request, **kwargs):   

    if 'activate' in kwargs:
    	request.session['activate_notifications'] = True
    if 'deactivate' in kwargs:
     	request.session['activate_notifications'] = False    	

    return HttpResponse('Toggle view notifications')

