Ext.define('popup.view.Viewport', {
    extend : 'Ext.container.Viewport',
    alias  : 'widget.mainViewport',

    layout : 'fit',

    items : [{
        xtype : 'form',
        title : 'Приложение загружено',
        bodyPadding : 10,
        items : [{
            xtype : 'button',
            text : 'Кнопка'
        }]
    }]

});